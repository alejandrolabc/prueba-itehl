package com.itehl.pruebaproductor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/***
 * Clase main del pruebaconsumidor
 * @author luis_balaguera
 *
 */
@SpringBootApplication
public class PruebaProductorApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaProductorApplication.class, args);
	}

}
