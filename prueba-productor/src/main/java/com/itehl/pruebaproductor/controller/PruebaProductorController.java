package com.itehl.pruebaproductor.controller;

import com.itehl.domain.*;
import com.itehl.pruebaproductor.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controlador con un metodo post, que injectando
 * un objeto de tipo KafkaTemplate template publica en
 * un topico kafka lo que recibe en el cuerpo del metodo
 *
 * @author luis_balaguera
 */
@RestController
@RequestMapping("/v1/user")
public class PruebaProductorController {

    @Value("${topic.name}")
    private String topicName;

    private final KafkaTemplate<String, UserCreated> kafkaTemplate;

    @Autowired
    public PruebaProductorController(KafkaTemplate<String, UserCreated> kafkaTemplate) {
        super();
        this.kafkaTemplate = kafkaTemplate;
    }

    /**
     * recibe un json con los parametros de la clase UserCreated
     * generada por avro, para posteriormente con el metodo send
     * del objeto kafkaTemplate publicarlo en el topico respectivo,
     * enviandole como llave el userId del objeto recibido
     * @param user
     * @return Response
     */
    @PostMapping
    public ResponseEntity<Response> publishMessage(@RequestBody UserCreated user) {
        try {
            kafkaTemplate.send(topicName, user.getUserId(), user);
            return ResponseEntity.ok(new Response(200, "se publico correctamente"));
        } catch (Exception e) {
            return ResponseEntity.status(500).body(new Response(500, e.getCause().toString()));
        }
    }
}
