package com.itehl.pruebaproductor.response;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

/**
 * Estructura general de respuesta que se enviará al cliente
 *
 * @author luis_balaguera
 */
@ToString
@Data
@Api(tags = "Respuesta genérica compartida en todo el API")
public class Response {

    @ApiModelProperty(value = "Código de la respuesta del servicio, típicamente un código HTTP status", required = false)
    private Integer code;
    @ApiModelProperty(value = "Mensaje asociado a la respuesta de un servicio", required = false)
    private String message;

    public Response(Integer code, String message) {
        super();
        this.code = code;
        this.message = message;
    }

}
