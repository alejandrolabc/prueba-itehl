package com.itehl.pruebaconsumidor.listener;

import com.itehl.domain.*;
import com.itehl.pruebaconsumidor.model.User;
import com.itehl.pruebaconsumidor.repository.UserCreatedRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

/**
 * Servicio que escucha el topico, injecta un objeto
 * de tipo userCreatedRepository para insertar en bd
 * el mensaje que reciba del topico
 *
 * @author luis_balaguera
 */
@Service
@Slf4j
public class KafkaConsumer {

    private final UserCreatedRepository userCreatedRepository;

    @Autowired
    public KafkaConsumer(UserCreatedRepository userCreatedRepository){
        super();
        this.userCreatedRepository = userCreatedRepository;
    }

    /**
     * metodo que escucha al topico userCreated,
     * para posteriormente cuando se publique algo en el
     * topico, por medio del esquema avro, setearlo
     * en un objeto User y asi poder insertarlo en una
     * coleccion de mongo, y una vez insertado hacer un commit manual
     * al topico, para que algun otro cliente que se conecte no procese
     * el mismo mensaje.
     *
     * @param record
     * @param ack
     * @return void
     */
    @KafkaListener(topics = "userCreated", groupId = "group_id", containerFactory = "kafkaManualAckListenerContainerFactory")
    public void consume(@Payload UserCreated record, Acknowledgment ack) {
        //muestra el contenido del topico
        log.info("consumed message: {}", record);
        User user = new User();
        user.setUserId(record.getUserId());
        user.setFirstName(record.getFirstName());
        user.setLastName(record.getLastName());
        user.setAddress(record.getAddress());
        user.setEmail(record.getEmail());
        user.setPhone(record.getPhone());
        user.setMobile(record.getMobile());
        //Almacena en mongodb
        userCreatedRepository.save(user);
        //hace commit manual al topico
        ack.acknowledge();
    }
}
