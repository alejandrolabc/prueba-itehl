package com.itehl.pruebaconsumidor.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Objeto dto con los parametros que
 * se guardaran en la coleccion userCreated
 *
 * @author luis_balaguera
 */
@Data
@NoArgsConstructor
@Document(collection = "userCreated")
public class User {
    @Id
    private String userId;
    private String firstName;
    private String lastName;
    private String address;
    private String email;
    private Integer phone;
    private String mobile;

}
