package com.itehl.pruebaconsumidor.repository;

import com.itehl.pruebaconsumidor.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Interfaz con la etiqueta repository
 * que permite hacer uso de los metodos del
 * MongoRepository para operar una coleccion de mongo
 *
 * @author luis_balaguera
 */
@Repository
public interface UserCreatedRepository extends MongoRepository<User, String> {
}
