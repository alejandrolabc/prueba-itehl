package com.itehl.pruebaconsumidor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/***
 * Clase main del pruebaconsumidor
 * @author luis_balaguera
 *
 */
@SpringBootApplication
public class PruebaConsumidorApplication {

    public static void main(String[] args) {
        SpringApplication.run(PruebaConsumidorApplication.class, args);
    }

}
