# PruebaJavaKafkaItehl

## [](#autor)Autor

-   **Luis Alejandro Balaguera**

## [](#versión)Versión

1.0

## [](#descripción)Descripción

La finalidad de este proyecto, es la de generar dos programas que funcionaran uno como productor y otro como consumidor de un tópico kafka, el primero enviara la información de un usuario mientras que el segundo al leer el tópico y ver la información del usuario la persistirá en una colección de mongo.

## [](#ambiente)Ambiente

A continuación, se describe el ambiente en que se diseñó esta versión del proyecto;


<table>
<tr><th>Descripción</th><th>Nombre</th><th>Versión</th></tr>
<tr><td>Sistema Operativo</td><td>Windows</td><td>10 x64</td></tr>
<tr><td>Gestor de dependencias</td><td>Maven</td><td>3.6.3</td></tr>
<tr><td>Framework</td><td>SpringBoot</td><td>2.4.5</td></tr>
<tr><td>IDE</td><td>Intellij IDEA</td><td>Community 2021.1</td></tr>
<tr><td>Conector Base de Datos</td><td>Mongodb</td><td>4.4</td></tr>
<tr><td>JDK</td><td>Java</td><td>1.8 x64</td></tr>
</table>

# [](#procesos)Procesos

## [](#instalación)Instalación

El proyecto se encuentra en un repositorio GitLab, para poderlo descargar se deben seguir los siguientes pasos:

1.  Tener instalado un cliente GIT
2.  Descargar el repositorio al local con la URL proporcionada
3.  Ubicar el branch master
4.  Abrir el proyecto en el IDE de preferencia

## [](#configuracion)Configuracion

Para que el microservicio de productor funcione, se debe configurar en el archivo application.yml la propiedad bootstrap.servers con la ruta del kafka y el puerto correcto en el que se esta ejecutando, así mismo se debe configurar la propiedad schema.registry.url con la ruta del registry y su respectivo puerto; y validar que en la carpeta target generated-source se encuentra la clase generada por avro.

Para que el servicio de consumidor funcione, se debe al igual que con el productor configurar las propiedades del bootstrap.servers y del schema.registry.url con los valores correspondientes, asi como configurar las propiedades correspondientes a mongodb, para este caso se creo una base de datos de nombre pruebaItehl, pero se puede configurar con cualquiera; y también se debe validar que en la carpeta target generated-source se encuentra la clase generada por avro.

## [](#compilación)Compilación

Una vez se descargue el proyecto, se deberan descargar las dependencias correspondientes y compilar el proyecto para generar la clase UserCreated en dado caso en que no se encuentre ya generada, si no deja compilar el proyecto por no encontrar la clase UserCreated, se debera comentar en el codigo todas las lineas o funciones donde sea llamada, para poder compilar y que se genere la clase en el targe y despues descomentar las lineas que se acabaron de comentar.

## [](#despliegue)Despliegue

Para poder realizar el despliegue de los microservicios se debe hacer lo siguiente

```
 1. Compilar el proyecto corriendo el comando mvn clean install
 2. Buscar la clase principal del proyecto y darle click derecho Run.
```

Estos pasos estan realizados con el IDE Intellij

## [](#conector)Conector

Para poder configurar la configuración del conector FileSinkConnector se corrio en un contenedor docker una imagen de confluence con el comando
```
docker pull confluentinc/cp-kafka
```

Posteriormente se descargo el archivo docker-compose.yml correspondiente de la pagina
```
https://docs.confluent.io/platform/current/quickstart/ce-docker-quickstart.html#ce-docker-quickstart
```

Una vez se corrio el confluence se configuraron dos archivos de properties el configurationConnector.properties y el connect-avro-standalone.properties los cuales se pegaron en el servidor donde docker corre el contenedor de connector (cnfldemos/cp-server-connect-datagen); se accede a este servidor y en la ruta donde se pegaron los archivos de propiedades se corre el comando
```
connect-standalone connect-avro-standalone.properties configurationConnector.properties
```
Para que este funcione se debe configurar en el connect-avro-standalone.properties las rutas correspondientes al bootstrap.servers y del schema.registry.url
